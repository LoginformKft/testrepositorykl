pageextension 50100 "Book List Extension" extends BookList
{
    layout
    {
        addlast(General)
        {
            field("Book Type"; Rec."Book Type")
            {
                ApplicationArea = All;
            }

            field(Weight; Rec.Weight)
            {

                ApplicationArea = All;
            }
        }

    }

    actions
    {

    }
}
enum 50100 BookTypeEnum
{
    Extensible = true;

    value(0; " ")
    {
        Caption = ' ';
    }
    value(1; Cartoon)
    {
        Caption = 'Mese';
    }
    value(2; Thriller)
    {
        Caption = 'Krimi';
    }
    value(3; "Nature")
    {
        Caption = 'Természet';
    }

}

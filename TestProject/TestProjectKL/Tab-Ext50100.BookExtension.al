tableextension 50100 "Book Extension" extends Book
{
    fields
    {
        field(50100; Weight; Decimal)
        {
            Caption = 'Weight';
            DataClassification = ToBeClassified;
        }

        field(50101; "Book Type"; enum BookTypeEnum)
        {
            DataClassification = ToBeClassified;
        }
    }
}
